# Python modules
import networkx as nx
import itertools as it
# Local modules
from abp.fancy import GraphState
from abp.util import xyz
    
def make_cubic_graph(dims):
    graph = nx.Graph()
    graph.dims = graph.n, graph.m, graph.l = n, m, l = dims
    coords = it.product(range(1, n+1), range(m), range(1, l+1))
    graph.add_nodes_from(coords)
    shift_one_xyz = lambda x, y, z: [(x+1, y, z), (x, y+1, z), (x, y, z+1)]
    for node in graph.nodes():
        for adj_node in shift_one_xyz(*node):
            if graph.has_node(adj_node): graph.add_edge(node, adj_node)
    return graph
   
def odd(i):
    return bool(i % 2)

def even(i):
    return not bool(i % 2)    
   
def is_even(node):
    return all(even(node[i]) for i in range(3))   
    
def is_odd(node):
    return all(odd(node[i]) for i in range(3))
    
def even_vertices(graph):
    return [node for node in graph.nodes() if is_even(node)]

def odd_vertices(graph):    
    return [node for node in graph.nodes() if is_odd(node)]
    
def even_edges(graph):
    return [node for node in graph.nodes() 
        if odd(sum(node)) and not is_odd(node)]
    
def odd_edges(graph):
    return [node for node in graph.nodes() 
        if even(sum(node)) and not is_even(node)]
        
def left_face(graph):
    return [node for node in graph.nodes() \
        if node[2] == 1 and odd(sum(node[:2]))]
    
def right_face(graph):
    return [node for node in graph.nodes() \
        if node[2] == graph.l and odd(sum(node[:2]))]
    
def measure_x_nodes(graph):
    return list(set(even_edges(graph) + odd_edges(graph)) \
        - set(left_face(graph) + right_face(graph)))

def measure_z_nodes(graph):
    return even_vertices(graph) + odd_vertices(graph)
    
def t_xx(graph, y):
    return [node for node in graph.nodes() 
        if all([odd(node[0]), node[1] == y, odd(node[2])])]
    
def t_zz(graph, x):
    return [node for node in graph.nodes() 
        if all([node[0] == x, even(node[1]), even(node[2])])]

def multiply(eigenvalues):
    return reduce(lambda x, y: x*y, eigenvalues)

def xx_eigenvalue(graph, y, x_results, z_results):
    if odd(y):
        raise ValueError("Please pick an even y")
    z_nodes = t_xx(graph, y+1) + t_xx(graph, y-1)
    x_nodes = list(set(t_xx(graph, y)) \
        - set(left_face(graph) + right_face(graph)))
    x_eigenvalue = multiply([x_results[node] for node in x_nodes])
    z_eigenvalue = multiply([z_results[node] for node in z_nodes])
    return x_eigenvalue * z_eigenvalue
    
def zz_eigenvalue(graph, x, x_results, z_results):
    if even(x):
        raise ValueError("Please pick an odd x")
    z_nodes = t_zz(graph, x+1) + t_zz(graph, x-1)
    x_nodes = t_zz(graph, x)
    x_eigenvalue = multiply([x_results[node] for node in x_nodes])
    z_eigenvalue = multiply([z_results[node] for node in z_nodes])
    return x_eigenvalue * z_eigenvalue    
        
def measure_x(lattice):
    x_nodes = measure_x_nodes(lattice)
    x_results = {}
    for node in x_nodes:
        x_results.update({node: 2 * lattice.measure_x(node) - 1})
    return x_results
    
def measure_z(lattice):
    z_nodes = measure_z_nodes(lattice)
    z_results = {}
    for node in z_nodes:
        z_results.update({node: 2 * lattice.measure_z(node) - 1})
    return z_results

def logical_x(lattice, y, qubit):
    if qubit == 'left':
        nodes = left_face(lattice)
    elif qubit == 'right':
        nodes = right_face(lattice)
    else:
        raise ValueError("Please pick a logical qubit between 'left' and 'right'")
    if odd(y):
        raise ValueError("Please pick an even lattice qubit")
    nodes = [node for node in nodes if odd(node[0])]
    for node in nodes:
        lattice.act_local_rotation(node, 'px')
        
def logical_z(lattice, x, qubit):
    if qubit == 'left':
        nodes = left_face(lattice)
    elif qubit == 'right':
        nodes = right_face(lattice)
    else:
        raise ValueError("Please pick a loqical qubit between 'left' and 'right'")
    if even(x):
        raise ValueError("Please pick an odd lattice qubit")
    nodes = [node for node in nodes if even(node[1])]
    for node in nodes:
        lattice.act_local_rotation(node, 'pz')   
    
def make_lattice_state(dims, colours=True):
    graph = make_cubic_graph(dims)
    gs = GraphState()
    gs.dims = gs.n, gs.m, gs.l = graph.n, graph.m, graph.l
    node_colours = {node: 'red' for node in graph.nodes()}
    if colours:
        black_nodes = {node: 'black' for node in graph.nodes() 
                    if is_odd(node) or is_even(node)}
        logical_nodes = {node: 'yellow' for node in graph.nodes() 
            if (node[2] == 1 or node[2] == graph.l) and odd((node[0] + node[1]))}
        node_colours.update(black_nodes)
        node_colours.update(logical_nodes)
    for node, colour in node_colours.items():
        gs.add_qubit(node, position=xyz(*node), color=colour)
        gs.act_hadamard(node)
    for a, b in graph.edges():
        gs.act_cz(a,b)
    return gs    

# TODO: Comment functions

if __name__ == '__main__':
    dims = 7, 7, 7
    
    gs = make_lattice_state(dims)
    gs.update()

    x_results = measure_x(gs)
    z_results = measure_z(gs)    