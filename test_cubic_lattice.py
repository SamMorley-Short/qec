# Local modules
import cubic_lattice as cub

def test_even_and_odd():
    odds = [cub.odd(i) for i in range(10)]
    evens = [cub.even(i) for i in range(10)]
    assert odds == [False, True] * 5
    assert evens == [True, False] * 5
    
def test_xx_correlations():
    dims = 7, 7, 7  
    gs = cub.make_lattice_state(dims)
    x_results = cub.measure_x(gs)
    z_results = cub.measure_z(gs)
    ys = [y for y in range(dims[1]) if cub.even(y)]
    for y in ys:
        bulk_xx_e_value = cub.xx_eigenvalue(gs, y, x_results, z_results)
        log_xx_nodes = [node for node in cub.t_xx(gs, y) 
            if node in cub.left_face(gs) + cub.right_face(gs)] 
        xx_mnt_result = cub.multiply([2 * gs.measure_x(node) - 1 
            for node in log_xx_nodes])
        assert bulk_xx_e_value * xx_mnt_result == 1

def test_zz_correlations():
    dims = 7, 7, 7  
    gs = cub.make_lattice_state(dims)
    x_results = cub.measure_x(gs)
    z_results = cub.measure_z(gs)
    xs = [x for x in range(1,dims[0]+1) if cub.odd(x)]
    for x in xs:
        bulk_zz_e_value = cub.zz_eigenvalue(gs, x, x_results, z_results)
        log_zz_nodes = [node for node in cub.left_face(gs) + cub.right_face(gs) 
            if node[0] == x] 
        zz_mnt_result = cub.multiply([2 * gs.measure_z(node) - 1 
            for node in log_zz_nodes])
        assert bulk_zz_e_value * zz_mnt_result == 1